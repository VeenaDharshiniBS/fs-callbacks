const fs = require('fs');

function problem2Soln(givenTxtFile) {
    fs.readFile(givenTxtFile, 'utf-8', (err, data) => {
        if (err)
            console.error(err);
        else {
            console.log("1. Done reading the given text file");
            fs.writeFile('uppercaseFile.txt', data.toUpperCase(), (err) => {
                if (err)
                    console.error(err);
                else {
                    console.log("2. Done writing to the uppercaseFile.txt in uppercase");
                    fs.appendFile("filenames.txt", 'uppercaseFile.txt \n', (err) => {
                        if (err)
                            console.error(err);
                        else {
                            fs.readFile('uppercaseFile.txt', 'utf-8', (err, data) => {
                                if (err)
                                    console.error(err);
                                else {
                                    console.log("3. Done reading the uppercaseFile.txt");
                                    fs.writeFile('lowercaseFile.txt', JSON.stringify(data.toLowerCase().replace(/(\r\n|\n|\r)/gm, "").split('. ').filter((sen) => sen != "")), (err) => {
                                        if (err)
                                            console.error(err);
                                        else {
                                            console.log("Done writing to the lowercaseFile.txt in lowercase");
                                            fs.appendFile('filenames.txt', 'lowercaseFile.txt \n', (err) => {
                                                if (err)
                                                    console.error(err);
                                                else {
                                                    fs.readFile('lowercaseFile.txt', 'utf-8', (err, data) => {
                                                        if (err)
                                                            console.error(err);
                                                        else {
                                                            console.log("4. Done reading the content in lowercaseFile.txt");
                                                            fs.writeFile('sortedFile.txt', JSON.stringify(JSON.parse(data).sort()), (err) => {
                                                                if (err)
                                                                    console.error(err);
                                                                else {
                                                                    fs.appendFile('filenames.txt', 'sortedFile.txt \n', (err) => {
                                                                        if (err)
                                                                            console.error(err);
                                                                        else {
                                                                            console.log("Done sorting the content");
                                                                            fs.unlink('lowercaseFile.txt', (err) => {
                                                                                if (err)
                                                                                    console.error(err);
                                                                                else {
                                                                                    fs.unlink('uppercaseFile.txt', (err) => {
                                                                                        if (err)
                                                                                            console.error(err);
                                                                                        else {
                                                                                            fs.unlink('sortedFile.txt', (err) => {
                                                                                                if (err)
                                                                                                    console.error(err);
                                                                                                else
                                                                                                    console.log("5. All newly created files are deleted");
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    })
                                                                }
                                                            });
                                                        }
                                                    })
                                                }

                                            })

                                        }
                                    })
                                }
                            })
                        }
                    });
                }
            });
        }
    });
}

module.exports = problem2Soln;



