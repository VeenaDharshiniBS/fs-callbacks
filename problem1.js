const fileSystem = require("fs");

function createAndDeleteDir(dirName) {
    let randomNum = Math.floor(Math.random() * 10 + 1);
    let createdFilesCount = 0;
    console.log(randomNum)

    fileSystem.mkdir(dirName, (err) => {
        if (err)
            console.error(err);
        else {
            for (let create = 1; create <= randomNum; create++) {
                fileSystem.writeFile(`${dirName}/file_${create}.json`, JSON.stringify({ a: 1, b: 2, c: 3 }), (err) => {
                    if (err)
                        console.error(err);
                    else {
                        createdFilesCount += 1;
                        console.log(`Created file_${create}.json out of ${randomNum}`)
                        if (createdFilesCount === randomNum) {
                            console.log("Starting delete operation")
                            for (let del = 1; del <= randomNum; del++) {
                                fileSystem.unlink(`${dirName}/file_${del}.json`, (err) => {
                                    if (err)
                                        console.error(err);
                                    else
                                        console.log(`Deleted file_${del}.json`);
                                });
                            }
                        }
                    }
                })
            }
        }
    })
}

module.exports = createAndDeleteDir;

